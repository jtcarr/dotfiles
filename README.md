# Justin's dotfiles and other configs
Dotfile tracking based on this [Hacker News](https://news.ycombinator.com/item?id=11071754) post and this [article](https://www.atlassian.com/git/tutorials/dotfiles
) by Nicola Paolucci.

## Requirements
- curl
- git
  
## Install
Setup config tracking in your $HOME directory by running
```shell
curl -Lks https://gitlab.com/jtcarr/dotfiles/-/snippets/2063980/raw/master/setup.sh | /bin/bash
```
