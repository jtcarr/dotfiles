# Set alt as mod and meta as super
set $mod Mod1
set $super Mod4


# Setting the used font
font pango:Hack Regular Nerd Font Complete Mono 9
font pango:DejaVu Sans Mono 9

# Borders and mouse
default_border pixel 2
hide_edge_borders smart
focus_follows_mouse no
mouse_warping none

# Kill focused window
bindsym $mod+Shift+q kill

# Reload the configuration file
bindsym $mod+Shift+c reload

# Restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# Exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"

# Lock computer
bindsym --release $super+l exec i3lock
bindsym --release $super+s exec i3lock && systemctl suspend


# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# Toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# Change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# Enter fullscreen mode for the focused container
bindsym $mod+f fullscreen

# Change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split


# Applications
####################
set $editor nvim
set $browser iridium-browser
set $filemanager termite -e ranger
set $terminal termite -e nvim
set $rofi "rofi -show run -font 'snap 10' -color-normal '#000000,#ffffff,#222222,#44bbff,#ffffff' -color-window '#000000,#ffffff'"

bindsym $mod+Return exec $terminal
bindsym $mod+Control+d exec $rofi
bindsym $mod+Control+e exec $editor
bindsym $mod+Control+b exec $browser
bindsym $super+e exec $filemanager


# Workspaces
####################
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10
bindsym $mod+Tab workspace back_and_forth

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10


# Split in horizontal orientation
bindsym $super+h split h

# Split in vertical orientation
bindsym $super+v split v


# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# change focus with tab keys
bindsym $super+Tab focus right
bindsym $super+Shift+Tab focus left

# alternatively, you can use the cursor keys:
bindsym $mod+Left exec $control prev go
bindsym $mod+Right exec $control next go
bindsym $mod+Up exec $control up go
bindsym $mod+Down exec $control down go

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left exec $control prev move
bindsym $mod+Shift+Right exec $control next move
bindsym $mod+Shift+Up exec $control up move
bindsym $mod+Shift+Down exec $control down move


# Resize window (you can also use the mouse for that)
mode "resize" {
    # These bindings trigger as soon as you enter the resize mode

    # Pressing left will shrink the window’s width.
    # Pressing right will grow the window’s width.
    # Pressing up will shrink the window’s height.
    # Pressing down will grow the window’s height.
    bindsym h resize shrink width 3 px or 3 ppt
    bindsym j resize grow height 3 px or 3 ppt
    bindsym k resize shrink height 3 px or 3 ppt
    bindsym l resize grow width 3 px or 3 ppt

    # Same bindings, but for the arrow keys
    bindsym Left resize shrink width 3 px or 3 ppt
    bindsym Up resize grow height 3 px or 3 ppt
    bindsym Down resize shrink height 3 px or 3 ppt
    bindsym Right resize grow width 3 px or 3 ppt

    # Back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
    bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"


set $black	#002B36
set $white	#839496
set $gray	#CCCCCC
set $darkgray	#666666
set $green 	#799F26
set $blue	#4D73AA
set $purple	#8473A7
set $red	#B58900
set $orange	#E59847
set $cyan	#2AA198
set $dark	#00252E

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
    status_command i3status
    i3bar_command i3bar -t
    position top
    tray_output primary
    colors {
        statusline	$white
        background	$black

        # Colors            border      backgr.     text
        focused_workspace	$white	 	$white		$black
        active_workspace	#073642 	#073642 	#696f89
        inactive_workspace	#073642 	#073642     $white
        urgent_workspace  	$orange		$orange		$dark
    }
}






# # Examine later
# ###############
# # focus the parent container
# bindsym $mod+a focus parent

# # focus the child container
# bindsym $mod+d focus child

# # volume
# bindsym XF86AudioRaiseVolume exec amixer -D pulse sset Master 5%+ && pkill -RTMIN+1 i3blocks
# bindsym XF86AudioLowerVolume exec amixer -D pulse sset Master 5%- && pkill -RTMIN+1 i3blocks
# bindsym $mod+period exec amixer -D pulse sset Master 5%+ && pkill -RTMIN+1 i3blocks
# bindsym $mod+comma exec amixer -D pulse sset Master 5%- && pkill -RTMIN+1 i3blocks
# #
# # granular volume control
# bindsym $mod+XF86AudioRaiseVolume exec amixer -D pulse sset Master 1%+ && pkill -RTMIN+1 i3blocks
# bindsym $mod+XF86AudioLowerVolume exec amixer -D pulse sset Master 1%- && pkill -RTMIN+1 i3blocks
# # mute
# bindsym XF86AudioMute exec amixer sset Master toggle && killall -USR1 i3blocks
# # other keys
# bindsym XF86Calculator exec gnome-calculator
# bindsym XF86Explorer exec google-chrome
# #bindsym XF86Mail exec thunderbird -compose
# bindsym XF86HomePage exec nemo
# bindsym Print exec scrot '%Y-%m-%d_%T_scrot.png' -e 'mv $f ~/Pictures/screenshots/'
# # media keys
# bindsym $mod+Home exec curl http://localhost:2000/command/default/playpause
# bindsym $mod+Next exec curl http://localhost:2000/command/default/prev
# bindsym $mod+Prior exec curl http://localhost:2000/command/default/next
# bindsym $mod+Shift+s --release exec "scrot -s /tmp/screenshot-$(date +%F_%T).png -e 'xclip -selection c -t image/png < $f'"


# # screen layout keys
# bindsym $mod+F4 exec $script

# # startup programs
# # exec pulseaudio --start
# exec /home/benkaiser/.i3/update_background
# #exec --no-startup-id i3-msg 'workspace 0; exec geary'
# exec --no-startup-id i3-msg 'workspace 0; exec keepass'
# exec clipit
# exec CopyAgent
# #exec udiskie --tray
# #exec xflux -l -38.14 -g 144.35
# exec xscreensaver -no-splash
# #exec conky
# exec pa-applet
# exec nm-applet
# exec megasync
# exec amixer sset Master 20%
# #exec synergy &
# #exec --no-startup-id i3-msg 'workspace 1; exec google-chrome'
# #exec --no-startup-id compton -b --config /home/benkaiser/.i3/compton.conf
# exec compton --backend glx --paint-on-overlay --glx-no-stencil --vsync opengl-swc --unredir-if-possible
# exec xset s off &
# # sleep after 10 minutes of being awake and not doing anything
# exec xautolock -time 10 -locker "systemctl suspend" &

# # open certain windows in floating mode
# for_window [class="(?i)gnome-calculator"] floating enable
# for_window [class="(?i)lighthouse"] floating enable
# for_window [window_role="(?i)pop-up"] floating enable

# #class                  border  backgr. text    indicator
# client.focused			$white  $white  $dark    $dark
# client.unfocused		$dark   $dark   $white   $white
# client.focused_inactive	$dark   $dark   $white   $dark
# client.urgent			$orange $orange $dark    $white



# ####################
# ####################

# # -- Specific Window Rules --
# # Floating Calendar
# for_window [class="Yad"] floating enable
# # Floating Calculator
# for_window [title="Qalculate!"] floating enable
# # Move spotify to scratchpad when launched
# for_window [class="Spotify"] move scratchpad

# # -- Website Launcher --
# mode "$browser_links" {
# 	bindsym p exec xdg-open "https://play.pokemonshowdown.com"
# 	bindsym w exec xdg-open "https://www.wolframalpha.com"
# 	bindsym y exec xdg-open "https://www.youtube.com"
# 	bindsym i exec xdg-open "https://i3wm.org/docs/userguide.html"
# 	bindsym c exec xdg-open "https://www.chemeng.ntua.gr"
# 	bindsym m exec xdg-open "https://mycourses.ntua.gr"
# 	bindsym g exec xdg-open "https://mail.google.com/mail/u/0/#inbox"
# 	bindsym r exec xdg-open "https://www.reddit.com"
# 	bindsym l exec xdg-open "https://lolchess.gg/profile/eune/auroradraco"
# 	bindsym d exec xdg-open "https://www.dod.gr"
#   bindsym t exec xdg-open "https://www.twitch.tv"
#   bindsym Shift+g exec xdg-open "https://github.com"
# 	bindsym Shift+p exec xdg-open "https://calc.pokemonshowdown.com"
# 	bindsym Shift+c exec xdg-open "https://courses.chemeng.ntua.gr"
#   bindsym Shift+m exec xdg-open "https://www.messenger.com"
# 	bindsym Escape mode "default"
# }

# # Enter website launcher mode
# bindsym $mod+b mode "$browser_links"
