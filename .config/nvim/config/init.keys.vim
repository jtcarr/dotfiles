" [TABS] Mappings for tabs """""""""""""""""""""""""""""""""""""""""""""
map tn :tabn<CR>                   " Tab next
map tp :tabp<CR>                   " Tab previous
map tm :tabm                       " Moves active tab to position #
                                   " So tm 3<cr> will move current tab to location 3
map tt :tabnew<CR>                 " Creates a new tab
map ts :tab split<CR>              " Opens contens of current tab in new tab
map t1 :silent! tabn 1<CR>         " Go to tab 1
map t2 :silent! tabn 2<CR>         " Go to tab 2
map t3 :silent! tabn 3<CR>         " Go to tab 3
map t4 :silent! tabn 4<CR>         " Go to tab 4
map t5 :silent! tabn 5<CR>         " Go to tab 5
map t6 :silent! tabn 6<CR>         " Go to tab 6

" [MISC] Save read-only files """"""""""""""""""""""""""""""""""""""""""
cmap w!! w !sudo tee "%"           " Save as sudo if read only file

" [MISC] Force quit """"""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <Leader>qq :q!<cr>        " Quit when you want to --- even with unsaved work

" [WINDOWS] Easy windows navigation """"""""""""""""""""""""""""""""""""
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" [POPUP] Map ctrl + movement keys for movement """"""""""""""""""""""""
inoremap <expr> <C-j> pumvisible() ? "\<C-n>" : "\<C-j>"
inoremap <expr> <C-k> pumvisible() ? "\<C-p>" : "\<C-k>"

" [WINDOWS] Splits """""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <Leader>vs <C-w>v         " Open vertical split
nnoremap <Leader>hs <C-w>h         " Open horizontal split
nnoremap <Leader>nv :vne<CR>       " Open new vertical split
nnoremap <Leader>nh :new<CR>       " Open new horizontal split

" [WINDOWS] Resize window with shift + and shift - """""""""""""""""""""
nnoremap + <c-w>5>
nnoremap _ <c-w>5<

" [MISC] Map ctrl + s to save """"""""""""""""""""""""""""""""""""""""""
map <c-s> :w<CR>
imap <c-s> <C-o>:w<CR>
"nnoremap <Leader>s :w<CR>

" [MISC] Sane movement """""""""""""""""""""""""""""""""""""""""""""""""
nnoremap j gj
nnoremap k gk

" [MISC] Yank to the end of the line """""""""""""""""""""""""""""""""""
nnoremap Y y$

" [MISC] Copy to system clipboard """"""""""""""""""""""""""""""""""""""
vnoremap <C-c> "+y

" [MISC] Paste from system clipboard with ctrl + v """""""""""""""""""""
inoremap <C-v> <Esc>"+p
"nnoremap <Leader>p "0p
"vnoremap <Leader>p "0p
"nnoremap <Leader>h viw"0p

" [MISC] Move to the end of yanked text after yank and paste """""""""""
nnoremap p p`]
vnoremap y y`]
vnoremap p p`]

" [MISC] Move selected lines up and down """""""""""""""""""""""""""""""
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" [MISC] Clear search highlight """"""""""""""""""""""""""""""""""""""""
nnoremap <Leader><space> :noh<CR>

" [MISC] Maps for indentation in normal mode """""""""""""""""""""""""""
nnoremap <tab> >>
nnoremap <s-tab> <<

" [MISC] Indenting in visual mode """"""""""""""""""""""""""""""""""""""
xnoremap <tab> >gv
xnoremap <s-tab> <gv

" [MISC] Jump to definition in vertical split """"""""""""""""""""""""""
nnoremap <Leader>] <C-W>v<C-]>

" [NERDTREE] Handle nerdtree toggle """"""""""""""""""""""""""""""""""""
" nmap <C-n> :NERDTreeToggle<CR>
