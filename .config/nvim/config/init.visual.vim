if has('syntax') && !exists('g:syntax_on')
  syntax enable                                          " Turn on syntax highlighting
endif

set background=dark                                      " Set background to dark
silent! colorscheme PaperColor                           " Set prefered color scheme
hi LineNr ctermfg=7 guifg=#818181                        " Brighten line numbers a litte bit for my bad eyesight


" Statusline / Color / Highlighting / Etc """"""""""""""""""""""""""""""
"" Example statusline
""set statusline=\ %{toupper(mode())}                     " Mode
""set statusline+=\ \│\ %4F                               " File path
""set statusline+=\ %1*%m%*                               " Modified indicator
""set statusline+=\ %w                                    " Preview indicator
""set statusline+=\ %r                                    " Read only indicator
""set statusline+=\ %q                                    " Quickfix list indicator
""set statusline+=\ %=                                    " Start right side layout
""set statusline+=\ %{&enc}                               " Encoding
""set statusline+=\ \│\ %y                                " Filetype
""set statusline+=\ \│\ %p%%                              " Percentage
""set statusline+=\ \│\ %l/%L                             " Current line number/Total line numbers
""set statusline+=\ \│\ %c\                               " Column number


" generic statusline config """"""""""""""""""""""""""""""""""""""""""""
hi StatusLine ctermbg=24 ctermfg=51 cterm=none guibg=#304050 guifg=#FFFFD7 gui=none " Active statusline
hi StatusLineNC ctermbg=237 ctermfg=242 guibg=#757575 guifg=#b2b2b2 " Inactive statusline

hi Norm ctermbg=39 ctermfg=235 guibg=#00AFFF guifg=#262626     " Normal
hi Inse ctermbg=120 ctermfg=235 guibg=#87FF87 guifg=#262626    " Insert
hi Visu ctermbg=98 ctermfg=235 guibg=#875FD7 guifg=#262626     " Visual
hi Repl ctermbg=229 ctermfg=235 guibg=#FFFFAF guifg=#262626    " Replace
hi Read ctermbg=124 ctermfg=88 guibg=#304050 guifg=#AF0000     " Read only
hi Modi ctermbg=124 ctermfg=230 guibg=#B22222 guifg=#EEE8AA    " file has been changed

if has('statusline')
  " start fresh
  let &stl  = ''

  " colored mode section at front
  let &stl .= '%#Norm#%{mode()==#"n"?"  NORMAL ":""}%*'
  let &stl .= '%#Inse#%{mode()==#"i"?"  INSERT ":""}%*'
  let &stl .= '%#Visu#%{mode()==#"v"?"  VISUAL ":""}%*'
  let &stl .= '%#Visu#%{mode()==#"V"?"  V·LINE ":""}%*'
  let &stl .= '%#Visu#%{mode()==#"\<C-V>"?"  V·BLOCK ":""}%*'
  let &stl .= '%#Repl#%{mode()==#"R"?"  REPLACE ":""}%*'

  " if more than one buffer tell what buffer it is
  let &stl .= '%{bufnr("$")>1?"  buf ".bufnr("$")." |":""}'

  " file name (I found the file path was often too long, a way around this?)
  " followed by if the file has been modified and/or is read only
  let &stl .= '%#Modi#%{&mod ? "⋆":""}%* %f %#Read#%r%*'

  " split point
  let &stl .= '%='

  " filetype
  let &stl .= '%y'

  " percentage through file, line number, column number
  let &stl .= ' | %p%% %l:%02.3c '
endif


"" lightline status line config """""""""""""""""""""""""""""""""""""""""
"let g:lightline = {
"  \ 'active': {
"  \    'left': [ [ 'mode', 'paste' ], [ 'readonly', 'filename', 'modified' ] ],
"  \    'right': [ [ 'lineinfo' ], [ 'percent' ], [ 'fileencoding', 'filetype' ] ]
"  \ },
"  \ 'inactive': {
"  \    'left': [ [ 'filename' ] ],
"  \    'right': [ [ 'lineinfo' ], [ 'percent' ] ]
"  \ },
"  \ 'tabline': {
"  \    'left': [ [ 'tabs' ] ],
"  \    'right': [ [ 'close' ] ]
"  \ },
"  \ 'tab': {
"  \    'active': [ 'tabnum', 'filename', 'modified' ],
"  \    'inactive': [ 'tabnum', 'filename', 'modified' ]
"  \ },
"  \ 'component': {
"  \    'mode': '%{lightline#mode()}',
"  \    'absolutepath': '%F', 'relativepath': '%f', 'filename': '%t', 'modified': '%M', 'bufnum': '%n',
"  \    'paste': '%{&paste?"PASTE":""}', 'readonly': '%R', 'charvalue': '%b', 'charvaluehex': '%B',
"  \    'spell': '%{&spell?&spelllang:""}', 'fileencoding': '%{&fenc!=#""?&fenc:&enc}', 'fileformat': '%{&ff}',
"  \    'filetype': '%{&ft!=#""?&ft:"no ft"}', 'percent': '%3p%%', 'percentwin': '%P',
"  \    'lineinfo': '%3l:%-2v', 'line': '%l', 'column': '%c', 'close': '%999X X ', 'winnr': '%{winnr()}'
"  \ },
"  \ 'component_visible_condition': {
"  \    'modified': '&modified||!&modifiable', 'readonly': '&readonly', 'paste': '&paste', 'spell': '&spell'
"  \ },
"  \ 'component_function': {},
"  \ 'component_function_visible_condition': {},
"  \ 'component_expand': {
"  \    'tabs': 'lightline#tabs'
"  \ },
"  \ 'component_type': {
"  \    'tabs': 'tabsel', 'close': 'raw'
"  \ },
"  \ 'component_raw': {},
"  \ 'tab_component': {},
"  \ 'tab_component_function': {
"  \    'filename': 'lightline#tab#filename', 'modified': 'lightline#tab#modified',
"  \    'readonly': 'lightline#tab#readonly', 'tabnum': 'lightline#tab#tabnum'
"  \ },
"  \ 'mode_map': {
"  \    'n': 'NORMAL', 'i': 'INSERT', 'R': 'REPLACE', 'v': 'VISUAL', 'V': 'V-LINE', "\<C-v>": 'V-BLOCK',
"  \    'c': 'COMMAND', 's': 'SELECT', 'S': 'S-LINE', "\<C-s>": 'S-BLOCK', 't': 'TERMINAL'
"  \ },
"  \ 'separator': { 'left': '', 'right': '' },
"  \ 'subseparator': { 'left': '│', 'right': '│' },
"  \ 'tabline_separator': {},
"  \ 'tabline_subseparator': { 'left': '│' },
"  \ 'enable': { 'statusline': 1, 'tabline': 1 },
"  \ '_mode_': {
"  \    'n': 'normal', 'i': 'insert', 'R': 'replace', 'v': 'visual', 'V': 'visual', "\<C-v>": 'visual',
"  \    'c': 'command', 's': 'select', 'S': 'select', "\<C-s>": 'select', 't': 'terminal'
"  \ },
"  \ 'mode_fallback': { 'replace': 'insert', 'terminal': 'insert', 'select': 'visual' },
"  \ 'palette': {},
"  \ 'winwidth': winwidth(0),
"  \ 'colorscheme': 'one',
"\ }


"" onedark settings """""""""""""""""""""""""""""""""""""""""""""""""""""
"if exists('g:color_name')
"  if g:colors_name == "onedark"
"    let g:onedark_terminal_italics = 1                       "Enable italic font
"    let s:bold_highlight_groups = ['Function', 'Statement', 'Todo', 'CursorLineNr', 'MatchParen', 'StatusLine']
"    for group in s:bold_highlight_groups
"      call onedark#extend_highlight(group, { 'gui': 'bold' })
"    endfor
"  endif
"endif
