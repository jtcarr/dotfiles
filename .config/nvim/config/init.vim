" If we don't have vim-plug then load it
if empty(glob($NVIM.'/autoload/plug.vim'))
  let s:ppath = $NVIM.'/autoload/plug.vim'
  let s:prepo = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  exe '!curl -fLo ' . expand(s:ppath) . ' --create-dirs ' . s:prepo
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Get desired plugins
call plug#begin($NVIM.'/plugged')

"Plug 'joshdick/onedark.vim'
" Plug 'sjl/badwolf'
Plug 'NLKNguyen/papercolor-theme'
" Plug 'romainl/Apprentice'
" Plug 'goatslacker/mango.vim'
" Plug 'morhetz/gruvbox'
" Plug 'w0ng/vim-hybrid'
" Plug 'rakr/vim-one'
" Plug 'tomasr/molokai'
" Plug 'junegunn/seoul256.vim'
" Plug 'ghifarit53/tokyonight-vim'
" Plug 'nightsense/cosmic_latte'
" Plug 'KeitaNakamura/neodark.vim'
" Plug 'gilgigilgil/anderson.vim'
" Plug 'jacoborus/tender.vim'
" Plug 'itchyny/lightline.vim'

"Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"Plug 'mattn/emmet-vim'
"Plug 'sheerun/vim-polyglot'
"Plug 'tpope/vim-surround'

"Plug 'Townk/vim-autoclose'

call plug#end()
