set nocompatible               " In case vim symlinked to this use vim mode
filetype plugin indent on      " Enable plugins and indents by filetype
set title                      " Change the terminal's title
set history=100                " Store a bit more of :cmdline history
set showcmd                    " In the status bar, show incomplete commands as they are typed
set noshowmode                 " Hide showmode because of the statusline don't need double
"set gdefault                   " Set global flag for search and replace - For :substitute, use the /g flag by default
set smartcase                  " Smart case search if there is uppercase
set ignorecase                 " case insensitive search
set mouse=a                    " Enable mouse usage
set showmatch                  " Highlight matching bracket briefly jump to it's match when inserting a bracket
set timeoutlen=1000 ttimeoutlen=0 " Reduce Command timeout for faster escape and O
set fileencoding=utf-8         " Set utf-8 encoding on write
set linebreak                  " Wrap lines at convenient points
set nowrap                     " Remove word wrap
set listchars=tab:\ \ ,trail:· " Set trails for tabs and spaces
set list                       " Enable listchars
set lazyredraw                 " Do not redraw on registers and macros
set backspace=indent,eol,start " Backspacing over everything
set clipboard=unnamed          " Yank to the system clipboard by default
set confirm                    " Ask to save buffer instead of failing when executing commands which close buffers
set cmdheight=2                " # of lines for the command window cmdheight=2 helps avoid 'Press ENTER...' prompts
set laststatus=2               " Show a status line, even if there's only one Vim window
set hidden                     " allow switching away from current buffer w/o writing
set splitright                 " Set up new vertical splits positions
set splitbelow                 " Set up new horizontal splits positions
set path+=**                   " Allow recursive search
set inccommand=split           " Show substitute changes immidiately in separate split
set fillchars+=vert:\│         " Make vertical split separator full line
set pumheight=15               " Maximum number of entries in autocomplete popup
set exrc                       " Allow using local vimrc
set secure                     " Forbid autocmd in local vimrc
set diffopt+=vertical          " start diff mode with vertical splits by default
set helpheight=30              " Set window height when opening Vim help windows
set visualbell                 " Stop the anoying bell noises
set noerrorbells               " Really stop the anoying bell noises

set noswapfile                 " IMPORTANT: comment this line if you are working on a remote host
set undolevels=1000            " Store a lot of undo
set undofile

set nostartofline              " Keep cursor in same column for long-range motion cmds
set incsearch                  " Incremental search
set hlsearch                   " Highlight pattern matches as you type
set ignorecase                 " Ignore case when using a search pattern
set smartcase                  " Override 'ignorecase' when pattern has upper case character
set autochdir                  " Automatically change window's cwd to file's dir

set t_Co=256                   " Nvim says it doesn't use t_xx but this affects MS Windows terminal colors

" Set defaults for tabbing and spacing
set shiftwidth=4               " Autoindent indents 4 spaces
set softtabstop=4              " Use soft tabstops at 4 spaces
set tabstop=4                  " Use tabstops at 4 spaces
set expandtab                  " Expand tabs to spaces
set smartindent                " Enable smart indentation
set autoindent                 " Always set autoindenting on
set shiftround                 " round to 'shiftwidth' for "<<" and ">>"


if has('folding')
  set nofoldenable           " When opening files, all folds open by default
  set foldmethod=syntax      " Fold based on indent level
  set foldlevelstart=10      " Open most folds by default
  set foldnestmax=10         " 10 nested fold max

  function! NeatFoldText()
    let line = ' ' . substitute(getline(v:foldstart), '^\s*"\?\s*\|\s*"\?\s*{{' . '{\d*\s*', '', 'g') . ' '
    let lines_count = v:foldend - v:foldstart + 1
    let lines_count_text = '| ' . printf("%10s", lines_count . ' lines') . ' |'
    let foldchar = matchstr(&fillchars, 'fold:\zs.')
    let foldtextstart = strpart('+' . repeat(foldchar, v:foldlevel*2) . line, 0, (winwidth(0)*2)/3)
    let foldtextend = lines_count_text . repeat(foldchar, 8)
    let foldtextlength = strlen(substitute(foldtextstart . foldtextend, '.', 'x', 'g')) + &foldcolumn
    return foldtextstart . repeat(foldchar, winwidth(0)-foldtextlength) . foldtextend
  endfunction
  set foldtext=NeatFoldText()     " Use a custom foldtext function
endif


set scrolloff=3                " number of screen lines to show around the cursor
let &showbreak = ' └► '        " string to put before wrapped screen lines
"set sidescrolloff=2           " min # of columns to keep left/right of cursor

"set scrolloff=8               " Start scrolling when we're 8 lines away from margins
set sidescrolloff=15
set sidescroll=10


augroup vimrc
  autocmd!
augroup END

autocmd vimrc BufWritePre * :call StripTrailingWhitespaces()            "Auto-remove trailing spaces on saves
autocmd vimrc InsertEnter * :set nocul                                  "Remove cursorline highlight when inserting
autocmd vimrc InsertLeave * :set cul                                    "Add cursorline highlight in normal mode
"autocmd vimrc FileType c   setlocal sw=4 sts=4 ts=4 colorcolumn=80,120                     "Set indentation to 4 for c
"autocmd vimrc FileType h   setlocal sw=4 sts=4 ts=4 colorcolumn=80,120                     "Set indentation to 4 for c
"autocmd vimrc FileType py  setlocal sw=4 sts=4 ts=4 colorcolumn=79,120                     "Set indentation to 4 for python
"autocmd vimrc FileType php setlocal sw=4 sts=4 ts=4 colorcolumn=80,120                     "Set indentation to 4 for php
autocmd vimrc FocusGained,BufEnter * checktime                          "Refresh file when vim gets focus

autocmd vimrc FocusLost,BufLeave * :call NorLineNumbers()               " Set line numbers to normal when not focused
autocmd vimrc FocusGained,BufEnter * :call RelLineNumbers()             " Set line numbers to relative when focused
autocmd vimrc InsertEnter * :call NorLineNumbers()                      " Set line numbers to normal when in insert mode
autocmd vimrc InsertLeave * :call RelLineNumbers()                      " Set line numbers to relative when not in insert mode


set wildmode=list:longest,full

" Stuff to ignore when tab completing
set wildignore=
set wildignore+=*.o,*.obj,*~,*.exe,*.com,*.elf,*.bin,*.out
set wildignore+=*.png,*.jpg,*.gif,*.webp
set wildignore+=*.mov,*.avi,*.mp4,*.mpg,*.mpeg,*.webm,*.sw?
set wildignore+=*.doc,*.xls,*.pdf
set wildignore+=*.bak,tags
set wildignore+=*.git*
set wildignore+=*cache*
set wildignore+=*logs*
set wildignore+=*node_modules/**
set wildignore+=*.gem
set wildignore+=log/**
set wildignore+=tmp/**
" Mac stuff just in case (useful for that time I had to use Mac for a job)
set wildignore+=.CFUserTextEncoding,*/.Trash/*,*/Applications/*,*/Library/*,*/Movies/*,*/Music/*,*/Pictures/*,.DS_Store

set wildmenu

if exists('&wildignorecase')
  set wildignorecase
endif

" If on MS Windows use powershell as terminal if it exists
if has("win32") || has("gui_win32")
  if executable("PowerShell")
    " Set PowerShell as the shell for running external ! commands
    " http://stackoverflow.com/questions/7605917/system-with-powershell-in-vim
    set shell=PowerShell
    set shellcmdflag=-ExecutionPolicy\ RemoteSigned\ -Command
    set shellquote=\ " shellxquote must be a literal space character.
    set shellxquote=
  endif
endif


" FUNCTIONS """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! StripTrailingWhitespaces()
  if &modifiable
    let l:l = line(".")
    let l:c = col(".")
    %s/\s\+$//e
    call cursor(l:l, l:c)
  endif
endfunction

function! NorLineNumbers()
  set nu
  set rnu!
endfunction

function! RelLineNumbers()
  set nu
  set rnu
endfunction
