" Try and make synchronization across systems easier - one set of files
" Also try to be more modular

" This init.vim should be in ~/init.vim for FreeBSD or linux and
" %USERPROFILE%\AppData\Local\nvim\init.vim for MS Windows

if has('win32')
  " If HOME is not set then set it to our user profile directory
  if expand($HOME) !~? 'user'
    silent !SETX HOME "\%USERPROFILE\%"
    let $HOME = expand($USERPROFILE)
  endif

  let $NVIM = $HOME.'/AppData/Local/nvim'
else
  let $NVIM = $HOME.'/.config/nvim'
endif

source $NVIM/config/init.vim
source $NVIM/config/init.general.vim
source $NVIM/config/init.plugins.vim
source $NVIM/config/init.keys.vim
source $NVIM/config/init.visual.vim
