# .bashrc
# source: https://gitlab.com/jtcarr/dotfiles

# If not running interactively, don't do anything
[[ $- != *i* ]] && return


# Load additional bash configurations
# Note: ~/.config/bash/bash_extra can be used for settings that one
#       might not want to be commited.
for b_config_file in $HOME/.config/bash/bash_{colors,exports,functions,aliases,extra,prompt}; do
    [ -r $b_config_file ] && [ -f $b_config_file ] && source $b_config_file
done
unset b_config_file


# Load various scripts or applications

# Load trueline
[ -s "$TRUELINE_DIR/trueline.sh" ] && \. "$TRUELINE_DIR/trueline.sh"

# Load nvm
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

# Make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"


# Add bash completions if they exist
# TODO: move these out to a seperate bash_completions file... maybe.

# Load standard bash completions
if ! shopt -oq posix; then
    if [ -f /etc/bash_completion ]; then
        source /etc/bash_completion
    fi
fi

# Load nvm completions
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

# Add tab completion for ssh hostnames based on ~/.ssh/config, ignore wildcards
# Is this handled in /etc/bash_completion _known_hosts and _service now?
[ -e $HOME/.ssh/config ] && complete -o default -o nospace -W "$(grep ^Host ~/.ssh/config | grep -v [?*] | cut -d ' ' -f2- | tr ' ' '\n')" scp sftp ssh;

# Add git completions if it exists
if [ -f $HOME/.config/git/git-completion.bash ]; then
    source $HOME/.config/git/git-completion.bash
fi


# Set some shell options
# https://wiki.bash-hackers.org/internals/shell_options

# Case-insesitive globbing (used in pathname expansion)
shopt -s nocaseglob

# Append to the history file, don't overwrite it
shopt -s histappend

# Check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS. ON by default anyway
shopt -s checkwinsize

# Autocorrect typos in path names when using cd
shopt -s cdspell

# No empty command completion
shopt -s no_empty_cmd_completion

# Set programable completion. ON by default anyway
shopt -s progcomp


# Add private user bin directory if it exists
if [ -d $HOME/bin ]; then
    PATH=$HOME/bin:$PATH
fi

# Add gobin if it exists
if [ -d $HOME/.go/bin ]; then
    PATH=$PATH:$GOBIN
fi

# Add rust to path for rustup
if [ -d $HOME/.cargo/bin ]; then
    PATH=$PATH:$HOME/.cargo/bin
fi
